package com.rivki.themealdb.presenter

import android.content.Context
import com.rivki.themealdb.network.RetrofitFactory
import com.rivki.themealdb.view.CommonView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.Dispatcher

class MealPresenter(private val view: CommonView, private val context: Context) {
    fun getAllMealsByCategory(category: String) {
        view.showLoading()
        val api = RetrofitFactory.create()
        val request = api.getAllMelasByCategory(category)
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = request.await()
                view.success(response)
            } catch (e: Exception) {
                view.error(e)
            }
            view.hideLoading()
        }
    }

    fun getMealsById(id: String) {
        view.showLoading()
        val api = RetrofitFactory.create()
        val request = api.getAllMealById(id)
        CoroutineScope(Dispatchers.Main).launch {
            try {
                val response = request.await()
                view.success(response.meals[0])
            } catch (e: Exception) {
                view.error(e)
            }
            view.hideLoading()
        }
    }
}