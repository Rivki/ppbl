package com.rivki.themealdb

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.rivki.themealdb.model.Meal
import com.rivki.themealdb.presenter.MealPresenter
import com.rivki.themealdb.ui.DessertFragment
import com.rivki.themealdb.ui.SeafoodFragment
import com.rivki.themealdb.ui.favorite.FavoriteContainerFragment
import com.rivki.themealdb.view.CommonView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var fragmentContainer: FrameLayout? = null
    fun addFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, fragment, fragment.javaClass.simpleName)
            .commit()
    }

    private val onBottomNavItemClickListener: BottomNavigationView.OnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.menu_seafood -> {
                    val fragment = SeafoodFragment.newInstance()
                    addFragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }

                R.id.menu_dessert -> {
                    val fragment = DessertFragment.newInstance()
                    addFragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }

                R.id.menu_star -> {
                    val fragment = FavoriteContainerFragment.newInstance()
                    addFragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val mHandler = Handler()

        val mPendingRunnable = Runnable {
            bottom_navigation.setOnNavigationItemSelectedListener(onBottomNavItemClickListener)
            val defaultFragment = DessertFragment.newInstance()
            addFragment(defaultFragment)
        }
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable)
        }
    }
}
