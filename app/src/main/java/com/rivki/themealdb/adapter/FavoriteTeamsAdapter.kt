package com.rivki.themealdb.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rivki.themealdb.R
import com.rivki.themealdb.database.entity.MealEntity
import kotlinx.android.synthetic.main.item_meals.view.*

class FavoriteTeamsAdapter(private val favorite: List<MealEntity>, private val listener: FavoriteListener) : RecyclerView.Adapter<FavoriteTeamsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_meals, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = favorite.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindModel(favorite[position], listener)
    }

    interface FavoriteListener{
        fun itemClicks(data : MealEntity)
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val tvNamaMakanan = itemView.textView2
        val imageMakanan = itemView.imageView2

        fun bindModel(mealEntity: MealEntity, listener: FavoriteListener){
            tvNamaMakanan.text = mealEntity.strMeal
            Glide.with(itemView.context).load(mealEntity.strMealThumb).into(imageMakanan)
            itemView.setOnClickListener {
                listener.itemClicks(mealEntity)
            }
        }
    }
}
