package com.rivki.themealdb.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.rivki.themealdb.R
import com.rivki.themealdb.model.MealModel
import kotlinx.android.synthetic.main.item_meals.view.*

class MealsListAdapter(private val meals : MutableList<MealModel>, private val listener: Listener) :
    RecyclerView.Adapter<MealsListAdapter.ViewHolder>() {

    interface Listener{
        fun onItemClick(meal: MealModel)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_meals, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = meals.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindModel(meals[position], listener)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val tvNamaMakanan = itemView.textView2
        val imageMakanan = itemView.imageView2

        fun bindModel(meal: MealModel, listener: Listener){
            tvNamaMakanan.text = meal.strMeal
            Glide.with(itemView.context).load(meal.strMealThumb).into(imageMakanan)
            itemView.setOnClickListener {
                listener.onItemClick(meal)
            }
        }
    }
}