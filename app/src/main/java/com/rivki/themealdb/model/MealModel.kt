package com.rivki.themealdb.model


import com.google.gson.annotations.SerializedName

data class MealModel(
    @SerializedName("idMeal")
    var idMeal: String,
    @SerializedName("strMeal")
    var strMeal: String,
    @SerializedName("strMealThumb")
    var strMealThumb: String
)