package com.rivki.themealdb.model


import com.google.gson.annotations.SerializedName

data class MealDetailResponse(
    @SerializedName("meals")
    var meals: List<MealDetailModel>
)