package com.rivki.themealdb.model


import com.google.gson.annotations.SerializedName

data class Meal(
    @SerializedName("meals")
    var meals: List<MealModel>
)