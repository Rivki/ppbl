package com.rivki.themealdb.model


import com.google.gson.annotations.SerializedName

data class MealDetailModel(
    @SerializedName("idMeal")
    val idMeal: String,
    @SerializedName("strCategory")
    var strCategory: String,
    @SerializedName("strInstructions")
    var strInstructions: String,
    @SerializedName("strMeal")
    var strMeal: String,
    @SerializedName("strMealThumb")
    var strMealThumb: String

)