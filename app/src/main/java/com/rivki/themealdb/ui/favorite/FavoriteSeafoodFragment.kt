package com.rivki.themealdb.ui.favorite


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.rivki.themealdb.R

/**
 * A simple [Fragment] subclass.
 */
class FavoriteSeafoodFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_seafood, container, false)
    }

    companion object {
        fun newInstance(): FavoriteSeafoodFragment = FavoriteSeafoodFragment()
    }


}
