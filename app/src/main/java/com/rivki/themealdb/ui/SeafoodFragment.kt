package com.rivki.themealdb.ui


import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.util.TypedValue
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.rivki.themealdb.R
import com.rivki.themealdb.adapter.MealsListAdapter
import com.rivki.themealdb.model.Meal
import com.rivki.themealdb.model.MealModel
import com.rivki.themealdb.presenter.MealPresenter
import com.rivki.themealdb.util.Constant
import com.rivki.themealdb.view.CommonView
import kotlinx.android.synthetic.main.fragment_seafood.*
import org.jetbrains.anko.startActivity

class SeafoodFragment : Fragment(),  CommonView, MealsListAdapter.Listener{
    lateinit var presenter: MealPresenter
    lateinit var adapter: MealsListAdapter
    var meals: MutableList<MealModel> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_seafood, container, false)
    }

    companion object{
        fun newInstance(): SeafoodFragment = SeafoodFragment()
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setMealsData()
    }

    private fun dpToPx(dp: Int): Int{
        val r = resources
        return Math.round(
            TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), r.displayMetrics
            ))
    }

    private fun setMealsData(){
        rv_meals.setHasFixedSize(true)
        val layoutManager = GridLayoutManager(activity!!.applicationContext, 2)
        rv_meals.layoutManager = layoutManager
        rv_meals.addItemDecoration(GridSpacingItemDecoration(2, dpToPx(8), true))
        rv_meals.itemAnimator = DefaultItemAnimator()
        adapter = MealsListAdapter(meals, this)
        rv_meals.adapter = adapter
        presenter = MealPresenter(this, activity!!.applicationContext)
        presenter.getAllMealsByCategory("Seafood")
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
    }

    override fun error(error: Throwable) {
    }

    override fun success(anyResponse: Any) {
        val dataResponse = anyResponse as Meal
        meals.clear()
        meals.addAll(dataResponse.meals)
        adapter.notifyDataSetChanged()
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
    }

    override fun onItemClick(meal: MealModel) {
        val intent = Intent(activity?.applicationContext, DetailActivity::class.java)
        intent.putExtra(Constant.KEY_IDMEAL, meal.idMeal)
        startActivity(intent)
    }

    inner class GridSpacingItemDecoration(private val spanCount: Int, private val spacing: Int, private val includeEdge: Boolean) : RecyclerView.ItemDecoration() {

        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
            val position = parent.getChildAdapterPosition(view) // item position
            val column = position % spanCount // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing
                }
                outRect.bottom = spacing // item bottom
            } else {
                outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing // item top
                }
            }
        }
    }
}
