package com.rivki.themealdb.ui.favorite


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.rivki.themealdb.R
import com.rivki.themealdb.adapter.FavoriteTeamsAdapter
import com.rivki.themealdb.database.entity.MealEntity

class FavoriteDessertFragment : Fragment(), FavoriteTeamsAdapter.FavoriteListener {

    var favMeals: MutableList<MealEntity> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_seafood, container, false)
    }

    companion object {
        fun newInstance(): FavoriteDessertFragment = FavoriteDessertFragment()
    }

    override fun itemClicks(data: MealEntity) {
        
    }
}
