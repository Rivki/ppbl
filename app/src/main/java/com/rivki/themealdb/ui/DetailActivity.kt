package com.rivki.themealdb.ui

import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.rivki.themealdb.R
import com.rivki.themealdb.database.database
import com.rivki.themealdb.database.entity.MealEntity
import com.rivki.themealdb.model.MealDetailModel
import com.rivki.themealdb.presenter.MealPresenter
import com.rivki.themealdb.util.Constant
import com.rivki.themealdb.view.CommonView
import kotlinx.android.synthetic.main.activity_detail.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.toast

class DetailActivity : AppCompatActivity(), CommonView {

    private var isFavorite: Boolean = false
    private lateinit var tvMeals: TextView
    private lateinit var tvCategory: TextView
    private lateinit var tvInstruction: TextView
    private lateinit var imgThumbnail: ImageView
    private lateinit var presenter: MealPresenter
    private lateinit var pbDetail: ProgressBar
    private lateinit var id: String
    private lateinit var mealDetail: MealDetailModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        tvMeals = tv_detail_meail
        tvCategory = tv_detail_category
        tvInstruction = tv_detail_instruction
        imgThumbnail = img_detail_meal
        pbDetail = pb_detail

        val intent = intent
        id = intent.getStringExtra(Constant.KEY_IDMEAL)
        presenter = MealPresenter(this, this)
        presenter.getMealsById(id)
        favoriteButton.setOnClickListener {
            if (isFavorite) removeFromFavorite() else addToFavorite()
            setFavorite()
        }
    }

    override fun showLoading() {
        pbDetail.visibility = View.VISIBLE
    }

    override fun error(error: Throwable) {
    }

    override fun success(anyResponse: Any) {
        mealDetail = anyResponse as MealDetailModel
        tvMeals.text = mealDetail.strMeal
        tvCategory.text = mealDetail.strCategory
        tvInstruction.text = mealDetail.strInstructions
        Glide.with(this).load(mealDetail.strMealThumb).into(imgThumbnail)
        favoriteState()
        setFavorite()
    }

    override fun hideLoading() {
        pbDetail.visibility = View.GONE
    }

    private fun addToFavorite() {
        try {
            database.use {
                insert(
                    MealEntity.TABLE_FAVORITE,
                    MealEntity.ID_MEAL to mealDetail.idMeal,
                    MealEntity.NAME_MEAL to mealDetail.strMeal,
                    MealEntity.IMAGE_MEAL to mealDetail.strMealThumb,
                    MealEntity.INSTRUCTION to mealDetail.strInstructions,
                    MealEntity.CATEGORY to mealDetail.strCategory
                )
            }
            toast("added favorite")
            isFavorite = true
        } catch (e: SQLiteConstraintException) {
            toast(e.localizedMessage)
        }
    }

    private fun removeFromFavorite() {
        try {
            database.use {
                delete(
                    MealEntity.TABLE_FAVORITE,
                    "(${MealEntity.ID_MEAL} = {id})",
                    "id" to mealDetail.idMeal
                )
            }
            isFavorite = false
        } catch (e: SQLiteConstraintException) {

        }
    }

    private fun favoriteState() {
        database.use {
            val result = select(MealEntity.TABLE_FAVORITE).whereArgs(
                "(${MealEntity.ID_MEAL} = {id})",
                "id" to mealDetail.idMeal
            )
            val favorite = result.parseList(classParser<MealEntity>())
            if (!favorite.isEmpty()) isFavorite = true
        }
    }

    private fun setFavorite() {
        if (isFavorite) {
            favoriteButton.setImageResource(R.drawable.ic_star_border)
        } else {
            favoriteButton.setImageResource(R.drawable.ic_star_white)
        }
    }
}
