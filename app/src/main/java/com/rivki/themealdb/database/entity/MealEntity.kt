package com.rivki.themealdb.database.entity

class MealEntity(
    var idMeal: String,
    var strMeal: String,
    var strMealThumb: String,
    var category: String,
    var instruction: String
) {
    companion object {
        const val TABLE_FAVORITE: String = "table_meal_favorite"
        const val ID: String = "id"
        const val ID_MEAL: String = "id_meal"
        const val NAME_MEAL: String = "name_meal"
        const val IMAGE_MEAL: String = "image_meal"
        const val CATEGORY: String = "category"
        const val INSTRUCTION: String = "instruction"
    }
}