package com.rivki.themealdb.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.rivki.themealdb.database.entity.MealEntity
import org.jetbrains.anko.db.*

class DatabaseHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "MealDB.db", null, 1) {
    companion object {
        private var instance: DatabaseHelper? = null

        @Synchronized
        fun getInstance(ctx: Context): DatabaseHelper {
            if (instance == null) {
                instance = DatabaseHelper(ctx.applicationContext)
            }
            return instance as DatabaseHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(
            MealEntity.TABLE_FAVORITE, true,
            MealEntity.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            MealEntity.ID_MEAL to TEXT + NOT_NULL,
            MealEntity.NAME_MEAL to TEXT,
            MealEntity.CATEGORY to TEXT,
            MealEntity.INSTRUCTION to TEXT,
            MealEntity.IMAGE_MEAL to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable(MealEntity.TABLE_FAVORITE)
    }
}

val Context.database: DatabaseHelper
    get() = DatabaseHelper.getInstance(applicationContext)