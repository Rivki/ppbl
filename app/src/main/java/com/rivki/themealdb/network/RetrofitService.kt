package com.rivki.themealdb.network

import com.rivki.themealdb.model.Meal
import com.rivki.themealdb.model.MealDetailResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface RetrofitService {
    @GET("filter.php")
    fun getAllMelasByCategory(@Query("c") type: String) : Deferred<Meal>
    
    @GET("lookup.php")
    fun getAllMealById(@Query("i") type: String) : Deferred<MealDetailResponse>
}